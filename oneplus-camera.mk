# Camera
PRODUCT_PACKAGES += \
    libcamera2ndk_vendor

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/configs/permissions/oplus_camera_default_grant_permissions_list.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/default-permissions/oplus_camera_default_grant_permissions_list.xml \
    $(LOCAL_PATH)/configs/permissions/privapp-permissions-oplus.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-oplus.xml \
    $(LOCAL_PATH)/configs/sysconfig/hiddenapi-package-oplus-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-oplus-whitelist.xml \
    $(LOCAL_PATH)/configs/permissions/oplus_google_lens_config.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/oplus_google_lens_config.xml \

# OnePlus framework
PRODUCT_PACKAGES += \
    oplus-fwk.kona

PRODUCT_BOOT_JARS += \
    oplus-fwk.kona

# OnePlus wrapper
PRODUCT_BOOT_JARS += \
    oplus-support-wrapper

# Frameworks
TARGET_USES_OPLUS_CAMERA := true

# Properties
PRODUCT_PRODUCT_PROPERTIES += \
    persist.vendor.camera.privapp.list=com.oplus.camera \
    ro.com.google.lens.oem_camera_package=com.oplus.camera \
    ro.com.google.lens.oem_image_package=com.google.android.apps.photos

PRODUCT_SYSTEM_EXT_PROPERTIES += \
    ro.oplus.camera.video_beauty.prefix=oplus.video.beauty. \
    ro.oplus.camera.video.beauty.switch=oplus.switch.video.beauty \
    ro.oplus.camera.speechassist=true \
    ro.oplus.system.camera.flashlight=com.oplus.motor.flashlight \
    ro.oplus.system.camera.name=com.oplus.camera \
    ro.oplus.system.gallery.name=com.google.android.apps.photos

PRODUCT_VENDOR_PROPERTIES += \
    vendor.camera.algo.jpeghwencode=0

# Sepolicy
BOARD_VENDOR_SEPOLICY_DIRS += vendor/oneplus/camera/sepolicy/vendor
SYSTEM_EXT_PRIVATE_SEPOLICY_DIRS += vendor/oneplus/camera/sepolicy/private

# Oplus Camera
$(call inherit-product, vendor/oneplus/camera/camera-vendor.mk)